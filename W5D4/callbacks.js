class Clock {
  constructor() {
    // 1. Create a Date object.
    let time = new Date();
    // 2. Store the hours, minutes, and seconds.
    this.hours = time.getHours();
    this.minutes = time.getMinutes();
    this.seconds = time.getSeconds();
    // 3. Call printTime.
    this.printTime();
    window.setTimeout( () => {
      this._tick();
    }, 1000);
    // 4. Schedule the tick at 1 second intervals.
  }

  prettyTime(ints) {
    if (ints < 10) {
      return `0${ints}`;
    } else {
      return `${ints}`;
    }
  }

  printTime() {
    // Format the time in HH:MM:SS
    // Use console.log to print it.
    console.log(`${this.prettyTime(this.hours)}:${this.prettyTime(this.minutes)}:${this.prettyTime(this.seconds)}`);
  }

  _tick() {
    // 1. Increment the time by one second.
    this.seconds++;
    if (this.seconds >= 60 ){
      this.seconds = 0;
      this.minutes ++;
    }
    if (this.minutes >= 60) {
      this.minutes = 0;
      this.hours ++;
    }
    if (this.hours >= 24) {
      this.hours = 0;
    }

    this.printTime();
    window.setTimeout( () => {
        this._tick();
      }, 1000);
    // 2. Call printTime.
  }
}

const clock = new Clock();
