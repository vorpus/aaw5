const readline = require("readline");

const reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

class Game {

  constructor() {
    this.stacks = [[3,2,1],[],[]];
  }

  promptMove(cb){
    let fromMove;
    let toMove;
    this.print();
    reader.question("Get a move(from stack):", function(fromMove) {
        reader.question("Get a move(to stack):", function(toMove){
          console.log(`from: ${fromMove} => to: ${toMove}`);
          console.log(this);
          this.move(fromMove, toMove);
        }.bind(this));
    }.bind(this));
  }

  isValidMove(fromMove, toMove) {
    reader.close();
    let fromStack = this.stacks[fromMove];
    let discToMove = fromStack[fromStack.length-1];

    let toStack = this.stacks[toMove];
    let discToTop = toStack[toStack.length-1];

    if (toStack.length === 0) {
      return true;
    } else if (fromStack.length ===0 ) {
      return false;
    } else if (discToTop > discToMove) {
      return true;
    } else {
      return false;
    }
  }

  move(fromMove, toMove) {
    let fromStack = this.stacks[fromMove];
    let discToMove = fromStack.pop();
    this.stacks[toMove].push(discToMove);
    if (!this.isWon()){
      this.promptMove();
    } else {
      console.log("You win");
      reader.close();
    }
  }

  print(){
    this.stacks.forEach( (stack) => {
      console.log(stack);
    });
  }

  isWon() {
    if (this.stacks[0].length === 0 && this.stacks[1].length === 0) {
      return true;
    } else if (this.stacks[0].length === 0 && this.stacks[2].length === 0) {
      return true;
    }
    return false;
  }

  run() {
    this.promptMove();
  }
}
let game = new Game();
game.run();
