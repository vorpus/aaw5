const readline = require('readline');

function addNumbers (sum, numsLeft, completionCallback) {
  if (numsLeft === 0) {
    completionCallback(sum);
  } else {
    const reader = readline.createInterface ({
      input: process.stdin,
      output: process.stdout
    });

    reader.question("gimme a number: ", function(numString1) {
      sum += parseInt(numString1);
      numsLeft--;
      reader.close();
      addNumbers(sum, numsLeft, completionCallback);
    });

  }
}

addNumbers(0, 3, sum => console.log(`Total Sum: ${sum}`));
