require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe "GET #new" do
    it "renders the login page" do
      get :new, link:{}

      expect(response).to render_template("new")
      expect(response).to have_http_status(200)
    end

  end

  describe "POST #create" do
    let!(:user) { User.create(username: "johndoe", password: "123456")}
    context "with invalid login parameters" do
      it "does not log in user with wrong password" do
        post :create, user: { username: "johndoe", password: "" }

        expect(response).to render_template("new")
        expect(flash[:errors]).to be_present
        expect(session[:session_token]).to be_nil
      end

      it "does not log in user with wrong username" do
        post :create, user: { username: "janedoe", password: "123456" }

        expect(response).to render_template("new")
        expect(flash[:errors]).to be_present
        expect(session[:session_token]).to be_nil
      end


    end

    context "with valid login parameters" do
      it "logs in user" do
        post :create, user: { username: "johndoe", password: "123456" }

        expect(response).to redirect_to(goals_url)
        expect(session[:session_token]).to eq(User.last.session_token)
      end
    end
  end

  describe "POST #delete" do
    let!(:user) { User.create(username: "johndoe", password: "123456")}

    it "logs out user" do
      post :create, user: { username: "johndoe", password: "123456" }
      post :destroy

      expect(session[:session_token]).to be_nil

    end
  end

end
