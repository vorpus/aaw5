require 'rails_helper'

RSpec.describe GoalsController, type: :controller do

  let!(:john) { User.create!(username:"johndoe", password:"123456") }
  let!(:jane) { User.create!(username: "janedoe", password:"123456") }
  let!(:goal) { Goal.create!(title: "A goal", body: "A body", user: john, private: true) }


  describe "Permission" do
    it "Redirect to login unless logged in" do
      get :index
      expect(response).to redirect_to(new_session_url)
    end
  end

  describe "private goals" do
    before do
      allow(controller).to receive(:current_user) { jane }
    end

    it "does not show other users' private goals" do
      get :show, id: goal.id
      expect(flash[:errors]).to be_present
      expect(response).to redirect_to(goals_url)
    end
  end

  describe "GET #index" do
    before do
      allow(controller).to receive(:current_user) { john }
    end

    it "shows goals when logged in" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe "GET #new" do
    before do
      allow(controller).to receive(:current_user) { john }
    end
    it "renders the new goal page" do
      get :new

      expect(response).to render_template("new")
      expect(response).to have_http_status(200)
    end
  end

  describe "POST #create" do
    before do
      allow(controller).to receive(:current_user) { john }
    end

    context "with invalid params" do
      it "validates input" do
        post :create, goal: { title: "", body: "", user: john }
        expect(response).to render_template("new")
        expect(flash[:errors]).to be_present
      end
    end

    context "with valid params" do
      it "creates new user with valid username and password" do
        post :create, goal: { title: "A goal", body: "A body", user: john }
        expect(response).to redirect_to(goal_url(Goal.last))
      end
    end
  end


  describe "#update" do
    before do
      allow(controller).to receive(:current_user) { john }
    end

    it "allows user to edit their own goal" do
      patch :update, id: goal.id, goal: {body: "changed", title: "changedtitle"}
      updated_goal = Goal.find(goal.id)
      expect(updated_goal.body).to eq("changed")
      expect(updated_goal.title).to eq("changedtitle")
    end

    it "allows user to mark a goal as completed" do
      patch :update, id: goal.id, goal: {complete: true}
      updated_goal = Goal.find(goal.id)
      expect(updated_goal.complete).to be_truthy
    end
  end

  describe "#update as wrong user" do
    before do
      allow(controller).to receive(:current_user) { jane }
    end
    it "does not allow to edit other users goals" do
      patch :update, id: goal.id, goal: {body: "changed", title: "changedtitle"}
      updated_goal = Goal.find(goal.id)
      expect(updated_goal.body).not_to eq("changed")
      expect(updated_goal.title).not_to eq("changedtitle")
    end
  end
end
