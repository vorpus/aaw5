require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "GET #new" do
    it "renders the new user page" do
      get :new, link:{}

      expect(response).to render_template("new")
      expect(response).to have_http_status(200)
    end
  end

  describe "POST #create" do
    context "with invalid params" do
      it "validates the presence of username and password" do
        post :create, user: { username: "", password: "" }
        expect(response).to render_template("new")
        expect(flash[:errors]).to be_present
      end
    end

    context "with valid params" do
      it "creates new user with valid username and password" do
        post :create, user: { username: "johndoe", password: "verysecure" }
        expect(response).to redirect_to(goals_url)
      end

      it "logs user in after correct username and password" do
        post :create, user: { username: "johndoe", password: "verysecure" }
        expect(session[:session_token]).to eq(User.last.session_token)
      end
    end


  end
end
