# == Schema Information
#
# Table name: goals
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  user_id    :integer
#  private    :boolean
#  complete   :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Goal, type: :model do
  context "Validation" do
    let!(:goal) { Goal.new() }
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:body) }
    it { should validate_presence_of(:user) }
    it { should belong_to(:user) }

    it "defaults to be not private and not completed" do
      expect(goal.private).to be_falsey
      expect(goal.complete).to be_falsey
    end
  end
end
