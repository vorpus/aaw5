# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  username        :string           not null
#  password_digest :string           not null
#  session_token   :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:username) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_presence_of(:session_token) }
  it { should validate_length_of(:password).is_at_least(6) }
  it { should have_many(:goals) }

  describe "session token" do
    it "has a session token" do
      user1 = User.create(username: "johndoe", password: "123456")
      expect(user1.session_token).not_to be_nil
    end

    it "updates session token" do
      user1 = User.create(username: "johndoe", password: "123456")
      cur_session = user1.session_token
      user1.generate_session_token
      expect(cur_session).not_to eq(user1.session_token)
    end

  end

  describe "password encryption" do
    it "does not save passwords to the database" do
      User.create!(username: "jack_bruce", password: "abcdef")
      user = User.find_by_username("jack_bruce")
      expect(user.password).not_to be("abcdef")
    end

    it "encrypts the password using BCrypt" do
      expect(BCrypt::Password).to receive(:create)
      User.new(username: "jack_bruce", password: "abcdef")
    end
  end
end
