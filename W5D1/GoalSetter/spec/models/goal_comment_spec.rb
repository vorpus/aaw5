# == Schema Information
#
# Table name: goal_comments
#
#  id         :integer          not null, primary key
#  author_id  :integer          not null
#  goal_id    :integer          not null
#  body       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe GoalComment, type: :model do
  context "Validation" do
    it { should validate_presence_of(:author) }
    it { should validate_presence_of(:goal) }
    it { should validate_presence_of(:body) }
    it { should belong_to(:author) }
    it { should belong_to(:goal) }
  end
end
