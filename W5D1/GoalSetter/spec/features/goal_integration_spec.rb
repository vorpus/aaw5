require 'spec_helper'
require 'rails_helper'

feature "goal creation agile user story" do
  let!(:user) { User.create(username: "johndoe", password: "123456") }
  let!(:goal) { Goal.create(title: "my fancy goal", body: "do something fancy", user: user)}
  scenario "has a new goal page" do
    log_in
    visit new_goal_url
    expect(page).to have_content("New goal")
  end

  scenario "create a new goal" do
    log_in
    click_on "new goal"
    fill_in "title", with: "title"
    fill_in "body", with: "a body"
    click_on "submit"
    expect(page).to have_content("title")
    expect(page).to have_content("a body")
  end

  scenario "update goal" do
    log_in
    visit goal_url(goal)
    click_on "edit"
    fill_in "title", with: "FANCYPANTS"
    fill_in "body", with: "a body"
    click_on "submit"
    expect(page).to have_content("FANCYPANTS")
    expect(page).to have_content("a body")
  end

  scenario "delete goal" do
    log_in
    visit goal_url(goal)
    click_on "delete"
    expect(page).not_to have_content("my fancy goal")
  end

  scenario "shows goals after login" do
    log_in
    expect(page).to have_content("my fancy goal")
  end

  scenario "shows details when clicking on goal" do
    log_in
    click_on "my fancy goal"
    expect(page).to have_content("do something fancy")
  end

end
