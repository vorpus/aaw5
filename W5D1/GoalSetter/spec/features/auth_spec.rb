require 'spec_helper'
require 'rails_helper'

feature "the signup process" do

  scenario "has a new user page" do
    visit new_user_url
    expect(page).to have_content("Sign up")
  end
  feature "signing up a user" do

    scenario "shows username on the homepage after signup" do
      visit new_user_url
      fill_in "username", :with => "johndoe"
      fill_in "password", :with => "123456"
      click_on "submit"
      expect(page).to have_content("johndoe")
    end

  end

end

feature "logging in" do
  let!(:user) { User.create(username: "johndoe", password: "123456") }

  scenario "shows username on the homepage after login" do
    log_in
    expect(page).to have_content("johndoe")
  end

end

feature "logging out" do
  let!(:user) { User.create(username: "johndoe", password: "123456") }
  scenario "begins with a logged out state" do
    visit goals_url
    expect(page).to have_content("Log in")
  end

  scenario "doesn't show username on the homepage after logout" do
    log_in
    click_on "Log out"
    expect(page).to_not have_content("johndoe")
  end

end
