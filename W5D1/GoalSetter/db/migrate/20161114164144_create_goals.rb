class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.string :title
      t.text :body
      t.integer :user_id
      t.boolean :private
      t.boolean :complete

      t.timestamps null: false
    end
  end
end
