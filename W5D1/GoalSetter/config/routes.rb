Rails.application.routes.draw do
  resources :goal_comments
  resources :user_comments
  resources :goals
  resources :users, only: [:new, :create]
  resource :session, only: [:new, :create, :destroy]
end
