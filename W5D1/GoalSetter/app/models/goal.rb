# == Schema Information
#
# Table name: goals
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  user_id    :integer
#  private    :boolean
#  complete   :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Goal < ActiveRecord::Base
  validates :title, :body, :user, presence: true
  validates :private, :complete, default: false

  belongs_to :user
end
