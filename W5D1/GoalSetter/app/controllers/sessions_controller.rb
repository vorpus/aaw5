class SessionsController < ApplicationController

  def create
    user = User.find_by_credentials(params[:user][:username], params[:user][:password])
    if user
      log_in!(user)
      redirect_to goals_url
    else
      flash.now[:errors] = ["Invalid username/password."]
      render :new
    end
  end

  def destroy
    current_user.try(:reset_session_token!)
    session[:session_token] = nil
    render :new
  end

end
