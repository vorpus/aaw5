class GoalsController < ApplicationController
  before_action :require_logged_in

  def new

    render :new
  end

  def create
    new_goal = Goal.new(goal_params)
    new_goal.user = current_user
    if new_goal.save
      redirect_to goal_url(new_goal)
    else
      flash.now[:errors] = new_goal.errors.full_messages
      render :new
    end
  end

  def edit
    @goal = Goal.find(params[:id])
    render :edit
  end

  def update
    goal = Goal.find(params[:id])

    if goal && goal.user == current_user
      if goal.update(goal_params)
        redirect_to goal_url(goal)
      else
        flash.now[:errors] = goal.errors.full_messages
        render :edit
      end
    else
      flash[:errors] = ["Not permitted to edit."]
      redirect_to goals_url
    end
  end

  def show
    @goal = Goal.find(params[:id])
    if @goal
      if @goal.private && current_user != @goal.user
        flash[:errors] = ["Private!!"]
        redirect_to goals_url
      else
        render :show
      end
    else
      flash[:errors] = ["Invalid goal"]
      redirect_to goals_url
    end
  end

  def destroy
    goal = Goal.find(params[:id])
    if goal.destroy
      redirect_to goals_url
    else
      flash[:errors] = goals.errors.full_messages
      redirect_to goals_url
    end
  end

  def index
    @goals = current_user.goals
    render :index
  end

  private
  def goal_params
    params.require(:goal).permit([:title, :body, :complete, :private])
  end
end
