
function sum() {
  let sumVar = 0;
  for (var i = 0; i < arguments.length; i++) {
    sumVar += arguments[i];
  }
  return sumVar;
}

console.log(sum(1, 5, 6, 2));


function sumRest(...args) {
  let sumVar = 0;
  for (var i = 0; i < args.length; i++) {
    sumVar += args[i];
  }
  return sumVar;
}

console.log(sumRest(1, 5, 6, 2));


Function.prototype.myBind = function () {
  let context = arguments[0];
  // let args = Array.slice.call(arguments, 1);
  let args = Array.from(arguments).slice(1);
  let func = this;
  return function() {
      let allArgs = args.concat(Array.from(arguments));
      func.call(context, ...allArgs);
  };
};

Function.prototype.myBind = function (context, ...bindArgs) {
  // let args = Array.slice.call(bindArgs, 1);
  let func = this;
  return function(...callArgs) {
      let allArgs = bindArgs.concat(callArgs);
      func.call(context, ...allArgs);
  };
};


class Cat {
  constructor(name) {
    this.name = name;
  }

  says(sound, person) {
    console.log(`${this.name} says ${sound} to ${person}!`);
    return true;
  }
}

const markov = new Cat("Markov");
const breakfast = new Cat("Breakfast");
markov.says("meow", "Ned");
markov.says.myBind(breakfast, "meow", "Kush")();
markov.says.myBind(breakfast)("meow", "a tree");
markov.says.myBind(breakfast, "meow")("Markov");
const notMarkovSays = markov.says.myBind(breakfast);
notMarkovSays("meow", "me");



function curriedSum(numArgs) {
  let numbers = [];

  function _curriedSum(num) {
    numbers.push(num);
    if (numbers.length === numArgs) {
      return numbers.reduce((a, b) => a + b);
    } else {
      return _curriedSum;
    }
  }

  return _curriedSum;
}


Function.prototype.curry = function (numArgs) {
  let args = [];
  let func = this;

  function _curry(...currentArgs) {
    args = args.concat(currentArgs);
    if (args.length < numArgs) {
      return _curry;
    } else {
      // return func.apply(this, args);
      return func(...args);
    }
  }

  return _curry;
};

function sum(...args) {
  return args.reduce((a, b) => a + b);
}
