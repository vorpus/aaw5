/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	const GameView = __webpack_require__(1);

	document.addEventListener("DOMContentLoaded", function() {
	  const gameCanvas = document.getElementById("game-canvas");
	  gameCanvas.width = window.innerWidth;
	  gameCanvas.height = window.innerHeight;
	  const ctx = gameCanvas.getContext("2d");
	  const gameView = new GameView(ctx);
	  gameView.start();
	});


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	const Game = __webpack_require__(2);

	function GameView(ctx) {
	  this.game = new Game();
	  this.ctx = ctx;
	}

	GameView.prototype.start = function() {
	  this.game.bindKeyHandlers();
	  setInterval(() => {
	    this.game.step();
	    this.game.draw(this.ctx);
	  }, 20);
	};

	module.exports = GameView;


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	const Asteroid = __webpack_require__(3);
	const Ship = __webpack_require__(6);

	function Game() {
	  this.asteroids = [];
	  this.bullets = [];
	  this.ship = new Ship(this, this.randomPosition());
	  this.addAsteroids();
	}

	Game.prototype.DIM_X = window.innerWidth;
	Game.prototype.DIM_Y = window.innerHeight;
	Game.prototype.NUM_ASTEROIDS = 15;

	Game.prototype.addAsteroids = function () {
	  for (let i = 0; i < this.NUM_ASTEROIDS; i++) {
	    this.asteroids.push(new Asteroid(this, this.randomPosition()));
	  }
	};

	Game.prototype.addBullet = function (bullet) {
	  this.bullets.push(bullet);
	};

	Game.prototype.randomPosition = function () {
	  return [(Math.random() * this.DIM_X), (Math.random() * this.DIM_Y)];
	};

	Game.prototype.draw = function (ctx) {
	  ctx.clearRect(0, 0, this.DIM_X, this.DIM_Y);
	  const allObjs = this.allObjects();
	  for (let i = 0; i < allObjs.length; i++) {
	    allObjs[i].draw(ctx);
	  }
	};

	Game.prototype.moveObjects = function () {
	  const allObjs = this.allObjects();
	  for (let i = 0; i < allObjs.length; i++) {
	    allObjs[i].move();
	  }
	};

	Game.prototype.step = function () {
	  this.moveObjects();
	  this.checkCollisions();
	};

	Game.prototype.wrap = function (pos) {
	  let wrappedPos = pos;
	  if (pos[0] > this.DIM_X) {
	    wrappedPos[0] = pos[0] - this.DIM_X;
	  }
	  if (pos[1] > this.DIM_Y) {
	    wrappedPos[1] = pos[1] - this.DIM_Y;
	  }
	  if (pos[0] < 0) {
	    wrappedPos[0] = pos[0] + this.DIM_X;
	  }
	  if (pos[1] < 0) {
	    wrappedPos[1] = pos[1] + this.DIM_Y;
	  }
	  return wrappedPos;
	};

	Game.prototype.checkCollisions = function() {
	  const allObjs = this.allObjects();
	  for (let i = 0; i < allObjs.length; i++) {
	    for (var j = 0; j < allObjs.length; j++) {
	      if(allObjs[i].isCollidedWith(allObjs[j])) {
	        // alert("COLLISION");
	        allObjs[j].relocate();
	      }
	    }
	  }
	};

	Game.prototype.allObjects = function () {
	  return this.asteroids.concat(this.bullets).concat(this.ship);
	};

	Game.prototype.remove = function (asteroid) {
	  this.asteroids.splice(this.asteroids.indexOf(asteroid), 1);
	};

	Game.prototype.bindKeyHandlers = function () {
	  // debugger
	  key('a', () =>  this.ship.power([-1,0]) );
	  key('w', () =>  this.ship.power([0,-1]) );
	  key('s', () =>  this.ship.power([0,1]) );
	  key('d', () =>  this.ship.power([1,0]) );
	  key('space', () => this.ship.fireBullet() );
	};

	module.exports = Game;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	const Util = __webpack_require__(4);
	const MovingObject = __webpack_require__(5);
	const Ship = __webpack_require__(6);

	function Asteroid(game, pos) {
	  // debugger
	  MovingObject.call(this,
	    {
	      game: game,
	      pos: pos,
	      radius: this.RADIUS,
	      color: this.COLOR,
	      vel: Util.randomVec(this.SPEED)
	    }
	  );
	}

	Util.inherits(Asteroid, MovingObject);

	Asteroid.prototype.isCollidedWith = function (otherObject) {
	  if( otherObject instanceof Ship &&
	    (Util.distance(this.pos, otherObject.pos) <
	    (this.radius + otherObject.radius))
	  ) {
	    return true;
	  }
	  return false;
	};



	Asteroid.prototype.RADIUS = 25;
	Asteroid.prototype.COLOR = "#494f56";
	Asteroid.prototype.SPEED = 5;

	module.exports = Asteroid;


/***/ },
/* 4 */
/***/ function(module, exports) {

	const Util = {

	  inherits(childClass, parentClass) {
	    function Surrogate() {}
	    Surrogate.prototype = parentClass.prototype;
	    childClass.prototype = new Surrogate();
	    childClass.prototype.constructor = childClass;
	  },

	  // Return a randomly oriented vector with the given length.
	  randomVec(length) {
	    const deg = 2 * Math.PI * Math.random();
	    return Util.scale([Math.sin(deg), Math.cos(deg)], length);
	  },

	  // Scale the length of a vector by the given amount.
	  scale (vec, m) {
	    return [vec[0] * m, vec[1] * m];
	  },

	  distance(pos1, pos2) {
	    return Math.sqrt(
	      Math.pow(pos1[0] - pos2[0], 2) +
	      Math.pow(pos1[1] - pos2[1], 2)
	    );
	  }

	};

	module.exports = Util;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	const Util = __webpack_require__(4);

	function MovingObject (options) {
	  this.pos = options.pos;
	  this.vel = options.vel;
	  this.radius = options.radius;
	  this.color = options.color;
	  this.game = options.game;
	}

	MovingObject.prototype.draw = function(ctx) {
	  ctx.fillStyle = this.color;
	    ctx.beginPath();

	    ctx.arc(
	      this.pos[0],
	      this.pos[1],
	      this.radius,
	      0,
	      2 * Math.PI,
	      false
	    );

	    ctx.fill();
	};

	MovingObject.prototype.move = function() {
	  this.pos[0] += this.vel[0];
	  this.pos[1] += this.vel[1];
	  this.pos = this.game.wrap(this.pos);
	};

	MovingObject.prototype.isCollidedWith = function (otherObject) {
	  // if(
	  //   Util.distance(this.pos, otherObject.pos) <
	  //   (this.radius + otherObject.radius)
	  // ) {
	  //   return true;
	  // }
	  // return false;

	};

	module.exports = MovingObject;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	const Util = __webpack_require__(4);
	const MovingObject = __webpack_require__(5);
	const Game = __webpack_require__(2);
	const Bullet = __webpack_require__(7);

	function Ship (game, pos) {
	  MovingObject.call(this, {
	    game: game,
	    pos: pos,
	    radius: this.RADIUS,
	    color: this.COLOR,
	    vel: [0, 0]
	  });
	}

	Util.inherits(Ship, MovingObject);

	Ship.prototype.relocate = function () {
	  this.pos = this.game.randomPosition();
	  this.vel = [0, 0];
	};

	Ship.prototype.power = function (impulse) {
	  this.vel[0] += 2*impulse[0];
	  this.vel[1] += 2*impulse[1];
	};

	Ship.prototype.fireBullet = function () {
	  const bullet = new Bullet(this.game, this.pos, this.bulletVelocity());
	  this.game.addBullet(bullet);
	};

	Ship.prototype.bulletVelocity = function() {
	  return Util.scale(this.vel, 3);
	};

	Ship.prototype.RADIUS = 25;
	Ship.prototype.COLOR = "#c67a0f";

	module.exports = Ship;


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	const Util = __webpack_require__(4);
	const MovingObject = __webpack_require__(5);

	function Bullet (game, pos, vel) {
	  MovingObject.call(this, {
	    game: game,
	    pos: pos,
	    radius: this.RADIUS,
	    color: this.COLOR,
	    vel: vel
	  });
	}

	Util.inherits(Bullet, MovingObject);

	Bullet.prototype.RADIUS = 3;
	Bullet.prototype.COLOR = "#f229bf";

	module.exports = Bullet;


/***/ }
/******/ ]);