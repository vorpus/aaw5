const GameView = require('./lib/game_view.js');

document.addEventListener("DOMContentLoaded", function() {
  const gameCanvas = document.getElementById("game-canvas");
  gameCanvas.width = window.innerWidth;
  gameCanvas.height = window.innerHeight;
  const ctx = gameCanvas.getContext("2d");
  const gameView = new GameView(ctx);
  gameView.start();
});
