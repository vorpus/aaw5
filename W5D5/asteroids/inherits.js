Function.prototype.inherits = function(superClass) {
  function Surrogate() {}
  Surrogate.prototype = superClass.prototype;
  this.prototype = new Surrogate();
  this.prototype.constructor = this;
};

function MovingObject () {}

MovingObject.prototype.drift = function () {
  console.log("Inertia is a property of matter");
};

function Ship () {}
Ship.inherits(MovingObject);

Ship.prototype.blastOff = function () {
  console.log("Blast off!!!");
};

function Asteroid () {}
Asteroid.inherits(MovingObject);

// const asteroid = new Asteroid();
// const ship = new Ship();
