const Util = require('./util.js');
const MovingObject = require('./moving_object.js');
const Game = require('./game.js');
const Bullet = require('./bullet.js');

function Ship (game, pos) {
  MovingObject.call(this, {
    game: game,
    pos: pos,
    radius: this.RADIUS,
    color: this.COLOR,
    vel: [0, 0]
  });
}

Util.inherits(Ship, MovingObject);

Ship.prototype.relocate = function () {
  this.pos = this.game.randomPosition();
  this.vel = [0, 0];
};

Ship.prototype.power = function (impulse) {
  this.vel[0] += 2*impulse[0];
  this.vel[1] += 2*impulse[1];
};

Ship.prototype.fireBullet = function () {
  const bullet = new Bullet(this.game, this.pos, this.bulletVelocity());
  this.game.addBullet(bullet);
};

Ship.prototype.bulletVelocity = function() {
  return Util.scale(this.vel, 3);
};

Ship.prototype.RADIUS = 25;
Ship.prototype.COLOR = "#c67a0f";

module.exports = Ship;
