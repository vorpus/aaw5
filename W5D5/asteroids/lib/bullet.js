const Util = require('./util.js');
const MovingObject = require('./moving_object.js');

function Bullet (game, pos, vel) {
  MovingObject.call(this, {
    game: game,
    pos: pos,
    radius: this.RADIUS,
    color: this.COLOR,
    vel: vel
  });
}

Util.inherits(Bullet, MovingObject);

Bullet.prototype.RADIUS = 3;
Bullet.prototype.COLOR = "#f229bf";

module.exports = Bullet;
