const Util = require('./util.js');
const MovingObject = require('./moving_object.js');
const Ship = require('./ship.js');

function Asteroid(game, pos) {
  // debugger
  MovingObject.call(this,
    {
      game: game,
      pos: pos,
      radius: this.RADIUS,
      color: this.COLOR,
      vel: Util.randomVec(this.SPEED)
    }
  );
}

Util.inherits(Asteroid, MovingObject);

Asteroid.prototype.isCollidedWith = function (otherObject) {
  if( otherObject instanceof Ship &&
    (Util.distance(this.pos, otherObject.pos) <
    (this.radius + otherObject.radius))
  ) {
    return true;
  }
  return false;
};



Asteroid.prototype.RADIUS = 25;
Asteroid.prototype.COLOR = "#494f56";
Asteroid.prototype.SPEED = 5;

module.exports = Asteroid;
