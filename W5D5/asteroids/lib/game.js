const Asteroid = require('./asteroid.js');
const Ship = require('./ship.js');

function Game() {
  this.asteroids = [];
  this.bullets = [];
  this.ship = new Ship(this, this.randomPosition());
  this.addAsteroids();
}

Game.prototype.DIM_X = window.innerWidth;
Game.prototype.DIM_Y = window.innerHeight;
Game.prototype.NUM_ASTEROIDS = 15;

Game.prototype.addAsteroids = function () {
  for (let i = 0; i < this.NUM_ASTEROIDS; i++) {
    this.asteroids.push(new Asteroid(this, this.randomPosition()));
  }
};

Game.prototype.addBullet = function (bullet) {
  this.bullets.push(bullet);
};

Game.prototype.randomPosition = function () {
  return [(Math.random() * this.DIM_X), (Math.random() * this.DIM_Y)];
};

Game.prototype.draw = function (ctx) {
  ctx.clearRect(0, 0, this.DIM_X, this.DIM_Y);
  const allObjs = this.allObjects();
  for (let i = 0; i < allObjs.length; i++) {
    allObjs[i].draw(ctx);
  }
};

Game.prototype.moveObjects = function () {
  const allObjs = this.allObjects();
  for (let i = 0; i < allObjs.length; i++) {
    allObjs[i].move();
  }
};

Game.prototype.step = function () {
  this.moveObjects();
  this.checkCollisions();
};

Game.prototype.wrap = function (pos) {
  let wrappedPos = pos;
  if (pos[0] > this.DIM_X) {
    wrappedPos[0] = pos[0] - this.DIM_X;
  }
  if (pos[1] > this.DIM_Y) {
    wrappedPos[1] = pos[1] - this.DIM_Y;
  }
  if (pos[0] < 0) {
    wrappedPos[0] = pos[0] + this.DIM_X;
  }
  if (pos[1] < 0) {
    wrappedPos[1] = pos[1] + this.DIM_Y;
  }
  return wrappedPos;
};

Game.prototype.checkCollisions = function() {
  const allObjs = this.allObjects();
  for (let i = 0; i < allObjs.length; i++) {
    for (var j = 0; j < allObjs.length; j++) {
      if(allObjs[i].isCollidedWith(allObjs[j])) {
        // alert("COLLISION");
        allObjs[j].relocate();
      }
    }
  }
};

Game.prototype.allObjects = function () {
  return this.asteroids.concat(this.bullets).concat(this.ship);
};

Game.prototype.remove = function (asteroid) {
  this.asteroids.splice(this.asteroids.indexOf(asteroid), 1);
};

Game.prototype.bindKeyHandlers = function () {
  // debugger
  key('a', () =>  this.ship.power([-1,0]) );
  key('w', () =>  this.ship.power([0,-1]) );
  key('s', () =>  this.ship.power([0,1]) );
  key('d', () =>  this.ship.power([1,0]) );
  key('space', () => this.ship.fireBullet() );
};

module.exports = Game;
