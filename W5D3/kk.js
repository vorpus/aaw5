function Kitten(name) {
  this.name = name;
  this.height = 5;

}

Kitten.prototype.speak = function() {
  console.log(`${this.name} says hi.`);
};
