function Student (firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.courses = [];
}

Student.prototype.name = function() {
  return `${this.firstName} ${this.lastName}`;
};

Student.prototype.courses = function() {
  return this.courses;
};

steve = new Student("Steve", "Smith");

Student.prototype.enroll = function(course) {
  if (this.courses.includes(course)) {
    console.log("you suck");
  } else {
    this.courses.push(course);
    course.students.push(this);
  }
};

Student.prototype.courseLoad = function() {
  let h = {};
  for (let i = 0; i < this.courses.length; i++) {
    if (h[this.courses[i].department]) {
      h[this.courses[i].department] += this.courses[i].credits;
    } else {
      h[this.courses[i].department] = this.courses[i].credits;
    }
  }
  return h;
};

function Course (name, department, credits) {
  this.name = name;
  this.department = department;
  this.credits = credits;
  this.students = [];
}

Course.prototype.addStudent = function(student) {
  student.enroll(this);
};

geometry = new Course("geometry", "math", 4);
algebra = new Course("algebra", "math", 3);
science = new Course("science", "science", 3);
steve.enroll(geometry);
steve.enroll(algebra);
steve.enroll(science);
