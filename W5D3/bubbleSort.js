function factors(number) {
  arr = [];
  for (let i = 1; i <= number; i++) {
    if (number % i === 0 ) {
      arr.push(i);
    }
  }
  return arr;
}

factors(24);
//[1, 2, 3, 4, 6, 8, 12, 24]

function bubbleSort(arr) {
  let sorted = false;
  while (!sorted) {
    sorted = true;
    for (let i = 0; i < arr.length - 1; i++) {
      if (arr[i] > arr[i+1]) {
        let temp = arr[i+1];
        arr[i+1] = arr[i];
        arr[i] = temp;
        sorted = false;
      }
    }
  }
  return arr;
}
bubbleSort([1,5,1,2,7,8,3,15,12,6]);
// [1, 1, 2, 3, 5, 6, 7, 8, 12, 15]

function substrings(string) {
  const subStrings = [];
  for(let i = 0; i < string.length; i++) {
    for(let j = i + 1; j < string.length + 1; j++) {
      subStrings.push(string.slice(i,j));
    }
  }
  return subStrings;
}

substrings("cat");
