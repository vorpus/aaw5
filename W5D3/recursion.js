function range (start, end) {
  if (end < start) {
      return [];
  } else {
    return [start].concat(range(start + 1, end));
  }
}

range(1,5);

function expa(b, n) {
  if (n === 0) {
    return 1;
  } else {
    return b * expa(b, n - 1);
  }
}

expa(3, 2);

function expb(b, n) {
  if (n === 0) {
    return 1;
  } else if (n === 1) {
    return b;
  } else if (n % 2 === 0) {
    let x = expb(b, n/2);
    return x * x;
  } else {
    let x = expb(b, (n - 1) / 2);
    return b * x * x;
  }
}

expb(4, 3);


function fibonacci(n) {
  if (n <= 2) {
    return [1, 1];
  } else {
    let prevFib = fibonacci(n-1);
    return prevFib.concat(prevFib[prevFib.length - 1] + prevFib[prevFib.length - 2]);
  }
}
fibonacci(30);

function bsearch(array, target) {
  // debugger
  let midpoint = Math.floor(array.length / 2);
  if (array.length <= 1) {
    if (array[0] !== target) {
      return NaN;
    } else {
      return 0;
    }
  } else if (array[midpoint] === target) {
    return midpoint;
  } else if (array[midpoint] > target) {
    return bsearch(array.slice(0, midpoint), target);
  } else {
    return midpoint + 1 + bsearch(array.slice(midpoint + 1), target);
  }
}

bsearch([1, 2, 3], 1);
bsearch([2, 3, 4, 5], 3);
bsearch([2, 4, 6, 8, 10], 6);
bsearch([1, 3, 4, 5, 9], 5);
bsearch([1, 2, 3, 4, 5, 6], 6);
bsearch([1, 2, 3, 4, 5, 6], 0);
bsearch([1, 2, 3, 4, 5, 7], 6);


function makeChange(target, coins) {
  if (coins.length < 1) {
    return [];
  } else {
    if (target >= coins[0]) {
      return [coins[0]].concat(makeChange(target - coins[0], coins));
    } else {
      return makeChange(target, coins.slice(1));
    }
  }
}
makeChange(14, [10, 7, 1]);


function mergeSort(array) {
  let midpoint = Math.floor(array.length / 2);
  if (array.length < 2) {
    return [array[0]];
  } else {
    return merge(mergeSort(array.slice(0, midpoint)), mergeSort(array.slice(midpoint)));
  }
}

function merge(arr1, arr2) {
  mergeArr = [];
  while (arr1.length !== 0 && arr2.length !== 0) {
    if (arr1[0] >= arr2[0]) {
      mergeArr.push(arr2.shift());
    } else {
      mergeArr.push(arr1.shift());
    }
  }
  return mergeArr.concat(arr1.concat(arr2));
}
// mergeSort([5,7,1,4,2]);

mergeSort([5,4,6,43,9,45,66,7,654,3]);


function subsets(arr) {
  if (arr.length === 0) {
    return [[]];
  } else {
    let prevSubsets = subsets(arr.slice(0,arr.length-1));
    for (let i = 0; i < arr.length; i++) {
      // console.log(`prevSubsets at i => ${prevSubsets[i]}`)
      prevSubsets.push(prevSubsets[i].concat(arr[arr.length - 1]));
    }
    return prevSubsets;
  }
}

subsets([1, 2]);
