function uniq(arr) {
  newArr = [];
  for (let i = 0; i < arr.length; i++) {
    if (newArr.includes(arr[i])) {
      continue;
    } else {
      newArr.push(arr[i]);
    }
  }
  return newArr;
}

function twoSum(arr) {
  couples = [];
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] + arr[j] === 0) {
        couples.push([i, j]);
      }
    }
  }
  return couples;
}

function myTranspose(arr) {
  let newArr = new Array(arr[0].length);
  for (let i = 0; i < arr[0].length; i++) {
    newArr[i] = new Array(arr.length);
  }

  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].length; j++) {
      newArr[i][j] = arr[j][i];
    }
  }
  return newArr;
}

rows = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8]
  ];
