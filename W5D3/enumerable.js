Array.prototype.myEach = function(cb) {
  for (let i = 0; i < this.length; i++) {
    cb(this[i]);
  }
};

function printMe(j) {
  console.log(j);
}




Array.prototype.myMap = function(cb) {
  let newArr = [];
  newArr.push(this.myEach(cb));
  return newArr;
};

function potato(j) {
  return j + 1;
}

[1,2,3,4].myMap(potato);


Array.prototype.myInject = function(cb) {
  let accumulator = this[0];

  const newVariable = this.slice(1);

  newVariable.myEach((arg) => { accumulator = cb(accumulator, arg); } );

  return accumulator;
};

function injectMe(accumulator, j) {
  return accumulator + j;
}
[1,2,3,4].myInject(injectMe);
