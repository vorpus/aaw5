require 'json'

class Flash


  def initialize(req)
    @cooky = req.cookies['_rails_lite_app_flash'] ? JSON.parse(req.cookies['_rails_lite_app_flash']) : {}
    @now = {}
  end

  def [](key)
    key = key.to_s.downcase
    @cooky[key]
  end

  def []=(key, val)
    @cooky[key] = val
  end

  def now

    key = key.to_s.downcase
  end

  # serialize the hash into json and save in a cookie
  # add to the responses cookies
  def store_flash(res)
    res.set_cookie("_rails_lite_app_flash", path: "/", value: @cooky.to_json)
  end


end
